use strict;
use warnings;

use ExtUtils::MakeMaker;

WriteMakefile(
	NAME => 'App::geencli',
	EXE_FILES => ['bin/geencli'],
);
