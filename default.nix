let
	pkgs = import <nixpkgs> {};
in
	pkgs.buildPerlPackage {
		name = "geencli";
		src = ./.;
		outputs = [ "out" ];
		buildInputs = [
			pkgs.perlPackages.HTMLTreeBuilderXPath
			pkgs.perlPackages.HTTPCookies
			pkgs.perlPackages.LWP
			pkgs.perlPackages.LWPProtocolHttps
		];
	}
